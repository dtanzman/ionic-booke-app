## App Description

This repo contains the source files for my `BookE` app. The purpose of this app, is to track what books you want to read and archive the ones you already read. The app allows for easy book searches and autofills the key information you need to buy or rent those books.

## How to use these files

To run this app, you will need to have the [ionicframework](http://ionicframework.com/getting-started/) installed. I suggest following the CLI instructions if you don't already have ionic version 3+.

### With the Ionic CLI installed:

To run the app, save the repository and cd into the `bookApp` folder then do the following:

Run it in browser. This will allow you to verify you installed it properly:

```bash
$ ionic serve
```

This requires xCode and/or Android Studio to run it on your ios device:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios --prod
```

Then from within xCode navigate to the packages platforms/ios/Read Me.xcodeproj to open the project and run it on your device or emulator.

For android, just replace `ios` with `android` and open in Android Studio. It's also possible to run directly from the CLI using these more [detailed deployment instructions](http://ionicframework.com/docs/intro/deploying/).

### Without Ionic CLI installed:

To install this app on any device by running it as a offline web page. The app now is able to use html5 appcache, allowing it to run offline and be saved to the devices `Home Screen`. Thank you Gil Broochian for the suggestion and contributions to make this type of installation possible.

To compile the web app files run the following command from the `bookApp` folder:

```bash
$ npm run ionic:build
```

To host run the following command from the new `www` folder:

```bash
$ python -m SimpleHTTPServer
```

Then open your device browser and go to your hosting machines IP address @ port 8000. Example: `10.0.0.6:8000`. Once you're able to view the app in the browser, save it to the devices `Home Screen`. At this point you can kill the local hosting and run the app solely from your phone indefinitely.

Note: This also bypasses Apple's 5 day limitation for self signed apps. 
