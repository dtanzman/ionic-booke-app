import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

// required to set back button text
import { ViewController } from 'ionic-angular';

import { GoogleBooksServiceProvider } from
    '../../providers/google-books-service/google-books-service';

import { Subject }    from 'rxjs/Subject';
//import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounce';
import 'rxjs/add/observable/timer';

import { Storage } from '@ionic/storage';

import { ToastController } from 'ionic-angular';

// to scroll to top
import { Content } from 'ionic-angular';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  // to scroll to top
  @ViewChild(Content) content: Content;

	items = []; //search results
  bookList = []; //toRead saved results

	filterOn = false;
	infiniteScrollOn = true;

  // search variables
	data: any;
  options: any;

  private infiniteScroll: any;

	// a subject to publish search terms
  private searchTerms = new Subject<any>();
  private dragItem = new Subject<any>();

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, private bookService: GoogleBooksServiceProvider, private storage: Storage, public toastCtrl: ToastController) {}


  ngOnInit() {
	  this.options = {
	    general: '',
	    author: '',
	    title: '',
	    subject: '',
	    startIndex: 0
	  };

	  // Get sample saved items a key/value pair
	  this.storage.get('searchItems').then((val) => {
	    console.log('Prior Search Items:', val);
		  });

    // get saved bookList array
    this.storage.get('dtanz_books').then((val) => {
      // prevent array from being changed to null
      if (val != null){
        // save prior array into this one
        this.bookList = val;
      }
    });

    // ignore multiple entries from itemDrag
    var debouncedItem = this.dragItem.debounce(() => Observable.timer(200));
    debouncedItem.subscribe(val => {
      console.log("Debounced Value", val);
      this.addBook(val);
    });

	  this.searchTerms
	    .switchMap(myOptions => {
	      console.log("Calling Search for:", myOptions);
	      return this.bookService.getBooks(myOptions); // pass options as parameter
	    })
	    .subscribe((result:any)=>
	          {
              if(result.totalItems == 0){
                console.log("Search Returned no Items");
              } else {
                // save results to data object
                this.data = result;
                // verify more results
                if(this.options.startIndex <= this.data.totalItems){
                  this.items = this.items.concat(result.items); //merge prior items with new array
                } else {
                console.log("end of search"); //toggle infinite off (turn back on when search new)
                this.infiniteScrollOn = false;
                }

                console.log(this.data);
                console.log(this.data.items);
              }

              // end infiniteScroll if active when get results
              if(this.infiniteScroll != null){
                this.infiniteScroll.complete();
              }

	          }, (err: any) => {
	          	console.log("Have Error", err);
	          });

  }//end ngOnInit

  onItemDrag(event,thisBook) {
    if(event._openAmount >= 120){
      // do action
      this.dragItem.next(thisBook);
    }
  }

  // Push a search term into the observable stream.
  search(options: any): void {
    //scroll to top
    this.content.scrollToTop();
    // changes reference of object so can't be changed after check
    let myOptions = Object.assign({}, options);
    // don't submit if blank inputs
    if(myOptions.general+myOptions.author+myOptions.title+myOptions.subject != ''){
      console.log("Search triggered for:", myOptions);
      this.searchTerms.next(myOptions);
      // reset properties (new search)
      this.items = [];
      this.options.startIndex = 0;
      this.infiniteScrollOn = true;
    }
  }

  addBook(object) {
    // target button
    var selectedBook = object;
    var bookExists = this.checkBookExists(selectedBook);

    // check if book exists
    console.log("Does Book Exist:", bookExists[0]);

    // add book to toRead list
    if (bookExists[0]) {
      // book already added
      this.presentToast("Already Added "+ selectedBook.volumeInfo.title);
        /*
        // replace archive book
        var oldIndex = bookExists[1];

        console.log("removed archive book Index:", oldIndex);
        // remove old book
        this.bookList.splice(oldIndex, 1);
        // add archive book
        console.log("Re-Added book:" + object.title);
        $scope.archive.push(selectedBook);
        */
    } else {
        // add new instance
        this.presentToast("Added "+ selectedBook.volumeInfo.title);
        var keyData = {
            "isbn": selectedBook.volumeInfo.industryIdentifiers[0].identifier,
            "thumbnail": selectedBook.volumeInfo.imageLinks.thumbnail,
            "title": selectedBook.volumeInfo.title,
            "subtitle": selectedBook.volumeInfo.subtitle,
            "authors": selectedBook.volumeInfo.authors,
            "publishedDate": selectedBook.volumeInfo.publishedDate,
            "textSnippet": selectedBook.searchInfo? selectedBook.searchInfo.textSnippet : "",
            "description": selectedBook.volumeInfo.description,
            "saveDate": Date(),
            "lastArchived": undefined
        };

        // update array
        this.bookList.push(keyData);
        console.log("Updated Book Array:", this.bookList);
    }
    // save updated array to memory
    this.storage.set('dtanz_books', this.bookList);
  }

  checkBookExists(thisBook) {
    var thisBookISBN = thisBook.volumeInfo.industryIdentifiers[0].identifier;
    var bookExists = false;
    var bookIndex;

    // start with top of toRead list
    for (var y = 0; y < this.bookList.length; y++) {
        var toReadBookISBN = this.bookList[y].isbn;

        if (thisBookISBN === toReadBookISBN) {
            console.log("Already Saved ISBN:" + thisBookISBN);

            bookExists = true;
            bookIndex = y;
        }
    };
    // return array of parameters
    return [bookExists, bookIndex];
  }

  doInfinite(infiniteScroll) {
    console.log('Begin Infinite Scoll');
    this.options.startIndex += 11;
    console.log("startIndex increments to " + this.options.startIndex);
    console.log("Search triggered for:", this.options);
    // search
    this.searchTerms.next(this.options);

    // make infiniteScroll functions accessable from other functions
    this.infiniteScroll = infiniteScroll;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentToast(message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'bottom'
    });

    toast.present();
  }

  filterToggle() {
    this.filterOn = !this.filterOn;
    //scroll to top
    this.content.scrollToTop();
  }

}
