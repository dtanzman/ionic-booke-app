import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// to simplify reordering
import { reorderArray } from 'ionic-angular';

// to push page location
import { SearchPage } from '../search/search';
import { DetailsPage } from '../details/details';

// to create search page modal
import { ModalController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { ToastController } from 'ionic-angular';

import { AlertController } from 'ionic-angular';

// to scroll to top
import { Content } from 'ionic-angular';
import { ViewChild } from '@angular/core';

//import Rx components from 'rxjs/Rx'
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounce';
import 'rxjs/add/observable/timer';

/**
 * Generated class for the ArchivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-archive',
  templateUrl: '../book-list/book-list.html',
})
export class ArchivePage {

  // to scroll to top
  @ViewChild(Content) content: Content;

  title = "Archive";

  filterOn = false;
  archiveButton = {"fill": false, "text": 'Un-Archive'};

  settings = {"sortSelection": ""}; //Note: default needs to be manual to prevent sorting when page loads

  items = [];
  bookList = [];
  archiveList = [];

  private dragItem = new Subject<any>();

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private storage: Storage, public toastCtrl: ToastController, public alertCtrl: AlertController) {}

  ngOnInit() {
    // ignore multiple entries from itemDrag
    var debouncedItem = this.dragItem.debounce(() => Observable.timer(200));
    debouncedItem.subscribe(val => {
      console.log("Debounced Value", val);
      this.archiveTrigger(val);
    });
  }

  ionViewDidEnter() {
    this.getStoredData();
  }

  onItemDrag(event,thisBook) {

    if(event._openAmount >= 180){

      this.dragItem.next(thisBook);
    }
  }

  getStoredData() {
    // get saved bookList
    this.storage.get('dtanz_books').then((val) => {
      // prevent array from being changed to null
      if (val != null){
        // save prior array into this one
        this.bookList = val;
      }
    });

    // get archived bookList
    this.storage.get('dtanz_archive').then((val) => {
      // prevent array from being changed to null
      if (val != null){
        // save prior array into this one
        this.archiveList = val;
        // show changes in current page (Archive)
        this.updatePageList(this.archiveList);
      }
    });

    // get saved settings
    this.storage.get('dtanz_settings').then((val) => {
      // prevent settings from being changed to null
      if (val != null){
        // save down prior settings
        this.settings = val;
      }
      console.log("getSettingsVal:", val);
    });
  }

  setStoredData() {
    // save changes to memmory
    this.storage.set('dtanz_books', this.bookList);
    this.storage.set('dtanz_archive', this.archiveList);
  }

  setSettingData() {
    // save settings to memmory
    this.storage.set('dtanz_settings', this.settings);
    console.log('setSettings',this.settings);
  }

  updatePageList(array) {
    // show changes in current array
    this.items = array;
  }

  archiveTrigger(thisBook) {
    console.log("Un-Archive book triggered", thisBook);

    //Check if book exists in Archive
    var bookExists = this.checkBookExists(thisBook);

    if(bookExists[0]) {
      //Book Exists in BookList
      var oldIndex = bookExists[1];
      // remove old book
      this.bookList.splice(oldIndex, 1);
      // replace with new version
      this.bookList.push(thisBook);
      this.presentToast("Re-Archived "+ thisBook.title);
    } else {
      //Book Doesn't Exist in Archive
      // add new book to archive
      this.bookList.push(thisBook);
      this.presentToast("Archived "+ thisBook.title);
    }

    // remove book from this array
    this.removeBook(thisBook);

    // save changes to memmory
    this.setStoredData();

    // reflect changes on page (Archive)
    this.updatePageList(this.archiveList);
  }

  trashBookTrigger(thisBook) {
    //confirm before delete
    const alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure you want to delete this book?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Delete clicked');
            //delete book
            this.removeBook(thisBook);
          }
        }
      ]
    });
    alert.present();
  }

  removeBook(thisBook) {
    //locate in book index
    var bookIndex = this.archiveList.indexOf(thisBook);
    if (bookIndex == -1) {
        console.log("Error: Book not found in toRead list");
    } else {
        // book exists
        console.log("Deleted " + thisBook.title);
        this.archiveList.splice(bookIndex, 1);
    }
    //save changes
    this.setStoredData();
  }

  checkBookExists(thisBook) {
    var thisBookISBN = thisBook.isbn;
    var bookExists = false;
    var bookIndex;

    // start with top of archiveList list
    for (var y = 0; y < this.bookList.length; y++) {
        var toReadBookISBN = this.bookList[y].isbn;

        if (thisBookISBN === toReadBookISBN) {
            console.log("ISBN Found:" + thisBookISBN);

            bookExists = true;
            bookIndex = y;
        }
    };
    // return array of parameters
    return [bookExists, bookIndex];
  }

  // create modal
  openModal() {
    // create modal off SearchPage template
    let myModal = this.modalCtrl.create(SearchPage);
    // on modal dismis refresh list to show latest changes
    myModal.onDidDismiss(() => { this.getStoredData(); });
    myModal.present();
  }

  // forwards page to detail page
  openNavDetailsPage(index) {
    this.navCtrl.push(DetailsPage, { items: this.items, index: index });
    console.log("Index ", index);
  }

  // triggers on reorder
  reorderItems(indexes) {
    // reorders item location
    this.items = reorderArray(this.items, indexes);
    // save changes
    this.archiveList = this.items;
    this.setStoredData();
  }

  filterToggle() {
    this.filterOn = !this.filterOn;
    //scroll to top
    this.content.scrollToTop();
  }

  presentToast(message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'bottom'
    });

    /*
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    */

    toast.present();
  }

}
