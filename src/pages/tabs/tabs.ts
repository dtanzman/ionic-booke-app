import { Component } from '@angular/core';

import { BookListPage } from '../book-list/book-list';
import { ArchivePage } from '../archive/archive';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = BookListPage;
  tab2Root = ArchivePage;

  constructor() {

  }

}
