import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  items = [];
  pageIndex;
  item;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	console.log("Passed Data:", navParams.data);
    //save down passed params
    this.items = navParams.data.items;
    this.pageIndex = navParams.data.index;
    this.item = this.items[this.pageIndex];
  }

}
