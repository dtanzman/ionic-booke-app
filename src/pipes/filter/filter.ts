import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
	transform(items: any[], searchText: string): any[] {
		//fail safes
		if(!searchText) return items;

		//ignore case
		searchText = searchText.toLowerCase();

		//filter
		return items.filter( 
			function(item) {
				// search objects by title
				return item.title.toLowerCase().includes(searchText);
			}
		);
	}
}
