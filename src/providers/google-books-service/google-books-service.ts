import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/finally';

/*
	Generated class for the GoogleBooksServiceProvider provider.

	See https://angular.io/guide/dependency-injection for more info on providers
	and Angular DI.
*/
@Injectable()
export class GoogleBooksServiceProvider {

	constructor(private http: Http) { }

	// get parameters as options object
	getBooks(options: any): Observable<any> {

		let params = {
			key: 'AIzaSyApBfqMrHg-cgIA2daaWv3EcuVjwShMFWA',
			filter: 'paid-ebooks',
			langRestrict: 'en',
			startIndex: options.startIndex
		};

		// build query based on option values
		var modifiers: { [key: string]: string } = {};
		if (options.title) {
			modifiers.intitle = options.title;
		}
		if (options.author) {
			modifiers.inauthor = options.author;
		}
		if (options.subject) {
			modifiers.subject = options.subject;
		}

		let query = String(encodeURIComponent(options.general));
		for (let modifer in modifiers) {
			query = query.concat(`+${modifer}:${encodeURIComponent(modifiers[modifer])}`);
		}

		// get results and output as json object
		return this.http
			.get(`https://www.googleapis.com/books/v1/volumes?q=${query}`, { params: params } )
			.map((res: Response) => res.json())
			.catch((e) => {
				return Observable.throw(
					new Error(`${ e.status } ${ e.statusText }`)
				);
			}).finally(()=>{
				console.log("service completed");
				var complete = true;
				return complete;
			});
	}
}
