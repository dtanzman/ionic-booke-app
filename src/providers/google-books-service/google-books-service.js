var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
/*
  Generated class for the GoogleBooksServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GoogleBooksServiceProvider = (function () {
    function GoogleBooksServiceProvider(http) {
        this.http = http;
    }
    // get parameters as options object
    GoogleBooksServiceProvider.prototype.getBooks = function (options) {
        var key = '&key=AIzaSyApBfqMrHg-cgIA2daaWv3EcuVjwShMFWA';
        var filter = '&filter=paid-ebooks';
        var langRestrict = '&langRestrict=en';
        // check if have option values
        var titleOption = options.title != '' ? '+intitle:' + options.title : '';
        var authorOption = options.author != '' ? '+inauthor:' + options.author : '';
        var subjectOption = options.subject != '' ? '+subject:' + options.subject : '';
        // get results and output as json object
        return this.http
            .get('https://www.googleapis.com/books/v1/volumes?q=' + options.general + titleOption + authorOption + subjectOption + '&startIndex=' + options.startIndex + langRestrict + filter + key)
            .map(function (res) { return res.json(); })
            .catch(function (e) {
            return Observable.throw(new Error(e.status + " " + e.statusText));
        });
    };
    return GoogleBooksServiceProvider;
}());
GoogleBooksServiceProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http])
], GoogleBooksServiceProvider);
export { GoogleBooksServiceProvider };
//# sourceMappingURL=google-books-service.js.map