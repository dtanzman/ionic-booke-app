import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { SearchPage } from '../pages/search/search';
import { BookListPage } from '../pages/book-list/book-list';
import { ArchivePage } from '../pages/archive/archive';
import { DetailsPage } from '../pages/details/details';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpModule } from '@angular/http';
import { GoogleBooksServiceProvider } from '../providers/google-books-service/google-books-service';

import { IonicStorageModule } from '@ionic/storage';

import { FilterPipe } from '../pipes/filter/filter';
import { SortPipe } from '../pipes/sort/sort';

@NgModule({
  declarations: [
    MyApp,
    SearchPage,
    BookListPage,
    ArchivePage,
    DetailsPage,
    TabsPage,
    FilterPipe,
    SortPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SearchPage,
    BookListPage,
    ArchivePage,
    DetailsPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GoogleBooksServiceProvider
  ]
})
export class AppModule {}
